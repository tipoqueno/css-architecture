/*doc
---
title: Lightbox
name: lightbox
category: 4.components
---

```html_example
<div class="thumbnail">
    <a href="#lightbox-thumb1" title="Clicking this link shows the lightbox">
        <img src="../assets/images/thumb-booking1.png" alt="">
    </a>
</div>
<section class="lightbox -fade" id="lightbox-thumb1" tabindex="-1" role="dialog" aria-labelledby="lightbox-label" aria-hidden="true">
    <div class="lightbox-inner">
        <a href="#!" class="lightbox-close" title="Close this lightbox" data-close="Close" data-dismiss="lightbox"></a>
        <img src="../assets/images/thumb-booking1.png" alt="">
    </div>
</section>
```
*/

/**
 * CSS Lightbox Configuration
 * http://drublic.github.com/css-modal
 *
 * @author Hans Christian Reinl - @drublic
 */

// Lightbox General Styles
$lightbox-max-width: 400px !default;
$lightbox-border-radius: 3px !default;
$lightbox-small-breakpoint: $screen-xs-min !default;

// Lightbox Plain Screen Styles
$lightbox-plain-screen-overlay: $white !default;

// Set high z-index to appear above all other content
$lightbox-layer: index($layers, lightbox) !default;

// Color configuration
$lightbox-border-color: $grey-50 !default;
$lightbox-inner-background: $white !default;
$lightbox-color: $grey !default;
$lightbox-mobile-header: $light-blue !default;

// Prevent overflow on HTML element
html {
    overflow-y: scroll;
    -webkit-overflow-scrolling: touch; // Native scroll momentum
}

// To prevent jumping <body> should not have any margin on top or bottom
.has-overlay {
    overflow: hidden;
    height: auto;

    > body {
        height: 100%;
        overflow: hidden;
    }
}

%lightbox {
    // Hide initially
    @include transform(translate3d(0, 100%, 0));

    // Maintain a fixed position
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: $lightbox-layer;
    overflow: hidden;
    opacity: 0;
    display: flex;
    align-items: center;
    justify-content: center;
    align-content: center;
    background-color: rgba($black, 0.75);
    width: 100%;
    height: 100%;

    // Show lightbox when requested
    &:target,
    &.is-active {
        @include transform(translate(0, 0));
        width: auto;
        height: auto;
        opacity: 1;
    }

    // Internet Explorer 8
    display: none\9;

    &.is-active {
        display: block\9;
        height: 100%\9;
        width: 100%\9;
    }

    // Overwrite IE8 hack for IE9 and IE10
    &:target,
    &.is-active {
        display: block\9;
    }

    // Content Area
    .lightbox-inner {
        background: $white;
        max-height: 80%;
        max-width: 80%;

        -webkit-overflow-scrolling: touch; // Native style momentum scrolling

        > img,
        > video,
        > iframe {
            width: 100%;
            height: auto;
            min-height: 300px;
        }

        > img {
            width: auto;
            max-width: 100%;
            vertical-align: middle;
        }

        iframe {
            display: block;
            width: 100%;
            border: 0;
            z-index: index($lightbox-layers, iframe);
            position: relative;
            min-height: 360px;
        }
    }

    // Content
    .lightbox-content {
        position: relative;
        overflow-x: hidden;
        overflow-y: auto;
        -webkit-overflow-scrolling: touch;

        > * {
            max-width: 100%;
        }
    }

    // Footer
    footer {
        border-top: 1px solid lighten($lightbox-border-color, 20);
        padding: 0 1.2em 18px;
        background: $grey;
        border-radius: 2px;
    }

    // A close button
    .lightbox-close {
        height: 1px; // Prevent close element to appear

        &:focus:after {
            outline: 1px dotted;
            outline: -webkit-focus-ring-color auto 5px;
        }

        // Background as close
        &:before {
            content: '';
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;

        }

        // Actual close button on lightbox
        &:after {
            content: '\00d7';
            position: absolute;
            top: -12px;
            right: -10px;
            color: $grey;
        }
    }

    // When screen isn't as wide as the lightbox anymore
    @media screen and (max-width: $lightbox-max-width + 40) {
        .lightbox-inner {
            width: auto;
            left: 20px;
            right: 20px;
            margin-left: 0;
        }

        .lightbox-close {
            &:after {
                margin-right: 0 !important;
                right: 20px;
            }
        }
    }

    // For small screens adjust the lightbox
    @media screen and (max-width: $lightbox-small-breakpoint) {
        -webkit-transform: translate(0, 400px); // Use px to work around Android 2.3 bug
        @include transform(translate3d(0, 100%, 0)); // And overwrite px if 3D transforms are supported
        @include transition(opacity 1ms .25s);

        display: block;
        right: auto;
        bottom: auto;

        &:target,
        &.is-active {
            width: 100%;
            height: 100%;

            // For IE we need to hide the close element to prevent overlay of other elements
            .lightbox-close {
                display: block;
            }
        }

        .lightbox-inner {
            -webkit-box-sizing: border-box;
               -moz-box-sizing: border-box;
                    box-sizing: border-box;

            top: 0;
            left: 0;
            right: 0;
            height: 100%;
            overflow: auto;
        }

        .lightbox-content {
            max-height: none;

            // Prevent text from breaking lightbox in mobile view
            -ms-word-break: break-all;
                word-break: break-all;
                word-break: break-word; // Non-standard for WebKit
            -webkit-hyphens: auto;
               -moz-hyphens: auto;
                    hyphens: auto;
        }

        .lightbox-close {
            display: none;
            right: auto;

            &:before {
                content: '';
                position: fixed;
                top: 0;
                left: 0;
                right: 0;
            }

            &:after {
                top: 5px !important;
                right: 5px;
                left: auto;
                margin-left: 0;
            }
        }
    }

    // For small heights
    @media screen and (max-height: 46em) and (min-width: $lightbox-small-breakpoint) {
        .lightbox-content {
            max-height: 340px;
            max-height: 50vh;
        }
    }

    @media screen and (max-height: 36em) and (min-width: $lightbox-small-breakpoint) {
        .lightbox-content {
            max-height: 265px;
            max-height: 40vh;
        }
    }

    // Stackable Lightboxes
    &.is-stacked {
        @include transform(translate(0, 0) scale(1, 1));
        opacity: 1;

        .lightbox-inner {
            @include animation(scaleDown .7s ease both);
        }

        // Hide overlay and close button
        .lightbox-close {
            opacity: 0;
        }

        // On small screens
        @media screen and (max-width: $lightbox-small-breakpoint) {
            @include animation(scaleDown .7s ease both);

            .lightbox-inner {
                @include animation(none);
            }

            .lightbox-close {
                opacity: 1;
            }
        }
    }
}

/**
 * All animations for CSS Lightbox
 *
 * Available:
 * - %lightbox--transition-fade (fade)
 * - %lightbox--transition-zoomIn (zooms in)
 * - %lightbox--transition-plainScreen (hides background)
 *
 * Usage:
 *
 * .selector {
 *      @extend %lightbox--transition-fade;
 * }
 *
 */

// Configuration:
$transition-duration: .4s !default;
$transition-size-start: scale(0) !default;
$transition-size-end: scale(1) !default;

// Fade in the Lightbox
%lightbox--transition-fade {
    @media screen and (min-width: $lightbox-small-breakpoint) {
        @include transition(opacity $transition-duration);
    }

    @extend %lightbox;
    @extend %lightbox-theme;
}

// Fade in and zoom in the lightbox
%lightbox--transition-zoomIn {
    @extend %lightbox;
    @extend %lightbox-theme;

    // Scale to zero
    .lightbox-inner {
        @include transform($transition-size-start);
        opacity: 0;
        @include transition(all $transition-duration);
    }

    .lightbox-close:before {
        @include transition(all $transition-duration);
        opacity: 0;
    }
    .lightbox-close:after {
        @include transform($transition-size-start);
        @include transition(all $transition-duration);
        opacity: 0;
    }

    // Show lightbox when requested
    &:target,
    &.is-active {
        .lightbox-inner {
            @include transform($transition-size-end);
            opacity: 1;
        }

        .lightbox-close:before {
            opacity: 1;
        }
        .lightbox-close:after {
            @include transform($transition-size-end);
            opacity: 1;
            // Move back to proper position
            top: 25px;

            @media screen and (max-width: $lightbox-small-breakpoint) {
                top: 5px;
                right: 5px;
                left: auto;
            }
        }
    }
}

// Fade in, zoom in and hide backgrund in the lightbox
%lightbox--transition-plainScreen {
    @extend %lightbox;
    @extend %lightbox-theme;
    @extend %lightbox-theme-plainScreen;
    @extend %lightbox--transition-zoomIn;

    .lightbox-inner {
        box-shadow: 0 0 15px -5px rgba($black, .25);
    }

    .lightbox-close:before {
        @include transition(all $transition-duration);
        background: $lightbox-inner-background;
        opacity: 0;
    }
    .lightbox-close:after {
        box-shadow: 0 -1px 10px -2px rgba($black, .2);
    }

    // Show lightbox when requested
    &:target,
    &.is-active {
        .lightbox-close:before {
            opacity: 1;
        }
        .lightbox-close:after {
            top: 23px;

            @media screen and (max-width: $lightbox-small-breakpoint) {
                top: 5px;
            }
        }
    }
}

/**
 * CSS Lightbox Themes
 * http://drublic.github.com/css-modal
 *
 * @author Hans Christian Reinl - @drublic
 */

/*
 * Global Theme Styles
 */

%lightbox-theme {
    color: $lightbox-color;
    line-height: 1.3;

    // Content Area
    .lightbox-inner {
        border-radius: $lightbox-border-radius;
        background: transparent;
        box-shadow: 0 0 20px rgba($black, .6);

        max-width: 100%;
        @include transition(max-width .25s linear, margin-left .125s linear);
    }

    // Header
    header {
        border-bottom: 1px solid $lightbox-border-color;
        padding: 0 1.2em;

        > h2 {
            margin: .5em 0;
        }
    }

    // Content
    .lightbox-content {
        padding: 15px 1.2em;
    }

    // Footer
    footer {
        border-top: 1px solid lighten($lightbox-border-color, 20);
        padding: 0 1.2em 18px;
        background: $white-10;
        border-radius: $lightbox-border-radius;
    }

    // A close button
    .lightbox-close {
        position: absolute;
        top: 1px;
        right: -34px;
        z-index: index($lightbox-layers, close-icon);
        cursor: pointer;

        // Actual close button on lightbox
        &:after {
            content: '\00d7';
            color: $white;
            padding: 11px;
            font-size: 40px;
            line-height: 20px;        }
    }

    // For small screens adjust the lightbox
    @media screen and (max-width: $lightbox-small-breakpoint) {
        .lightbox-close:before {
            background:  $lightbox-mobile-header;

            height: 3em;
            -webkit-box-shadow: 0 0 5px rgba($black, .4);
                    box-shadow: 0 0 5px rgba($black, .4);
        }

        .lightbox-inner {
            padding-top: 3em;
            -webkit-box-shadow: none;
                    box-shadow: none;
        }

        .lightbox-close {
            text-decoration: none;

            &:after {
                content: attr(data-close);
                font-size: 1em;
                padding: .5em 1em;
            }
        }
    }
}

/*
 * Plain Screen Theme Styles
 */
%lightbox-theme-plainScreen {

    .lightbox-inner {
        -webkit-box-shadow: 0 0 10px rgba($black, .25);
                box-shadow: 0 0 10px rgba($black, .25);
    }

    // A close button
    .lightbox-close {
        // Background as close
        &:before {
            background: $lightbox-plain-screen-overlay;
        }

        &:after {
            -webkit-box-shadow: 0px -4px 8px -1px rgba($black, .25);
                    box-shadow: 0px -4px 8px -1px rgba($black, .25);
        }
    }
}

/**
 * Apply the desired lightbox behavior to your container selector
 */
.lightbox {

    &-loader {
        z-index: index($lightbox-layers, loader);
        position: absolute;
        left: 47%;
        top: 28%;
    }

    &.-show {
        @extend %lightbox;
        @extend %lightbox-theme;
    }

    &.-fade {
        @extend %lightbox--transition-fade;
        @extend %lightbox-theme;
    }

    &.-fade, &.-show {

        .lightbox {

            &-inner {
                position: relative;
                margin: 30px;

                &.-col2 {
                    max-width: 600px;
                    @extend %flex-sb-fs;

                    & > * {
                        width: 50%;
                    }

                    &.-large {
                        max-width: 710px;
                    }
                }

                &.-info {
                    background: $info;

                    .lightbox-heading, p {
                        color: $white;
                    }

                    p {
                        line-height: 1.5;
                        font-size: 13px;
                    }
                }
                &.-video {
                    width: 640px;
                    height: 360px;
                    background: $black;
                }

                &.-matrix {
                    @include display(flex);
                    @include flex-wrap(wrap);

                    .lightbox-content {
                        width: 50%;
                        box-sizing: border-box;
                    }
                }

                &.-help {
                    width: 1000px;
                    background: $white;
                }
            }

            &-content {
                padding: 30px;
            }

            &-picture {
                border-radius: 0 0 $lightbox-border-radius 0;
                overflow: hidden;

                img {
                    max-width: 100%;
                    height: auto;
                }
            }

            &-heading {
                font-family: $roboto;
                margin-bottom: 15px;
            }
        }
    }
}


.lightbox {

    &-hover {
        background: $grey;
        position: relative;
        margin: 0;

        &::before {
            position: absolute;
            top: 0;
            right: 2em;
            left: 2em;
            z-index: -1;
            height: 3.5em;
            background: $white;
            content: '';
            @include transition(opacity 0.35s, transform 0.35s);
            @include transform(translate3d(0,4em,0) scale3d(1,0.023,1));
            @include transform-origin(50% 0);
        }
        img {
            @include transition-property(opacity);
            @include transition-duration(.35s);
            position: relative;
            display: block;
            min-height: 100%;
            max-width: 100%;
            opacity: 0.8;
        }

        figcaption {
            z-index: 1;
            color: $white;
            font-size: 1.25em;
            text-align: center;
            @extend %flex-center-center;
            @include flex-direction(column);
            @include backface-visibility(hidden);

            &, & > a {
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
            }
        }

        &-text,
        .icon {
            opacity: 0;
        }

        &-text {
            margin: 0;
            font-size: 100%;
            line-height: 1.5;
            letter-spacing: 1px;
            color: $white;
            @include transition(opacity 0.35s, transform 0.35s);
            @include transform(translate3d(0,-10px,0));
            -webkit-font-smoothing: antialiased;
            font-weight: bold;
            font-family: $roboto;
        }

        &:hover {

            img {
                opacity: 0.5;
            }

            .lightbox-hover-text,
            .icon {
                color: $grey-50;
                fill: $grey-50;
                opacity: 1;
            }

            .lightbox-hover-text {
                @include transform(translate3d(0,0,0));
            }

            figcaption {

                &::before {
                    opacity: 0.7;
                    @include transform(translate3d(0,5em,0) scale3d(1,1,1));
                }
            }
        }
    }

    &-header {
        border-bottom: 1px solid $white-15;
        padding: 10px 20px;
        background: $white-10;
        border-radius: 3px 0 0 0;
    }
}

